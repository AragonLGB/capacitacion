﻿using apiCapacitacion.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCapacitacion.Services
{
    public class ProcesosFacturas : ControllerBase
    {
        public ActionResult<int> GuardaFactura(Factura factura)
        {
            //Yo hago el guardado de la factura
            factura.IdFactura = 1;
            if (factura.IdFactura > 0)
            {
                return factura.IdFactura;
            }
            else
            {
                return NotFound("La factura no se guardo");
            }

        }
    }
}
