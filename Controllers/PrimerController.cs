﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiCapacitacion.Models;
using apiCapacitacion.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace apiCapacitacion.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrimerController : ControllerBase
    {
        public PrimerController()
        {
            //Ahora este es otro codigo
        }

        [HttpGet]
        public string ImprimeHola()
        {
            return "Hola puto";
        }

        [HttpGet("{nombre}/{apellido}/{edad}")]
        public string HolaUsuario(string nombre, string apellido, int edad)
        {
            return $"Hola {nombre} {apellido} de {edad} años";
        }

        [HttpPost]
        public ActionResult<int> GuardaFactura([FromBody]Factura factura)
        {
            ProcesosFacturas pf = new ProcesosFacturas();
            return pf.GuardaFactura(factura);
        }

        [HttpPut]
        public ActionResult<int> ActualizaFactura([FromBody] Factura factura)
        {
            ProcesosFacturas pf = new ProcesosFacturas();
            return pf.GuardaFactura(factura);
        }
    }
}
