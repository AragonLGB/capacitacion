﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCapacitacion.Models
{
    public class Factura
    {
        public int IdFactura { get; set; }
        public string Documento { get; set; }
        public decimal Total { get; set; }
    }
}
